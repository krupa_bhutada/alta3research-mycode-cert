#!/usr/bin/python3
from flask import Flask, redirect, url_for
app = Flask(__name__)

@app.route("/employee")
def emp_info(): #to provide employee information
    return '{ "name":"John", "age":"30", "designation":"Software Engineer"}'

@app.route("/award/<excellence>")
def hello_guest(excellence):
    return f"Thank you for {excellence} award"

if __name__ == "__main__":
   app.run(host="0.0.0.0", port=2224) # runs the application

