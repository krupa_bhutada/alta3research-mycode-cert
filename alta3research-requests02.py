#!/usr/bin/env python3
"""Alta3 Research  HTTP response parsing"""

# pprint makes dictionaries a lot more human readable
from pprint import pprint

# requests is used to send HTTP requests
import requests

URL = "https://api.publicapis.org/entries"

def main():
    """sending GET request, checking response"""
# Response is stored in "response" object
    response= requests.get(URL)

# check to see if the status is anything other than what we want, a 200 OK
    if response.status_code == 200:
# convert the JSON content of the response into a python dictionary
        result= response.json()
        pprint(result)

    else:
        print("That is not a valid URL.")

if __name__ == "__main__":
    main()
