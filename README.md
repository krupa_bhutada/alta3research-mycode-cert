# alta3research-mycode-cert

To obtain a Python and RESTful API Certification from Alta3 Research.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:79ad50dec4357a8085ab8fa82af00426?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:79ad50dec4357a8085ab8fa82af00426?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:79ad50dec4357a8085ab8fa82af00426?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/krupa_bhutada/alta3research-mycode-cert.git
git branch -M main
git push -uf origin main
```
# alta3research-mycode-cert (Project Title)

The objective of this is to get an opportunity to obtain a Python and RESTful API Certification from Alta3 Research. There is 1 public repository created. Inside it, there are 2 scripts named as alta3research-flask01.py and alta3research-requests02.py.The script alta3research-flask01.py demonstrates proficiency with the flask library.The script alta3research-requests02.py demonstrates proficiency with the requests HTTP library. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine
for development and testing purposes. See deployment for notes on how to deploy the project
on a live system.

## How to run alta3research-flask01.py script

Run on the command prompt
command - python3 alta3research-flask01.py
Go to aux1 terminal, add /employee to the url.
After that add /award/<excellence>

### Prerequisites

What things are needed to install the software and how to install them. For now, maybe copy in:
"How to Install the Language: "

## Built With

* [Python](https://www.python.org/) - The coding language used

## Authors

* **Krupa Bhutada** - (https://gitlab.com/krupa_bhutada/alta3research-mycode-cert)

